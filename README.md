# TuxWiki v2

Wiki niezbyt inteligentnego Tux-a na temat przedmiotów studiów inżynierskich na WWSI.

## Opis treści

Na tą chwilę repozytorium to **zaweira** notatki typu **stream of consciousness** sporządzone podczas wykładów oraz typu **cheatsheet**. 

### Czego to repozytorium nie zaweira

Repozytorium to **NIE ZAWIERA** bardziej połączonych notatek typu **wiki z backlinkami** (które ja sporządzam na własne potrzeby w aplikacji Obsidian). Jeśli uważasz, że takie notatki by Ci się do czegoś przydały **podenerwuj mnie na Disocrd aż to dodam**. 

Powodem braku tego na ten moment jest fakt, że hostowanie notatek obsidian jest *albo płatne (i dość ograniczone) albo uciążliwe w konfiguracji* wiec jeśli nie ma zainteresowania tym typem notatek nie będę ich tu udostępniał.

## Kontakt

Główną kontaktu na temat tej wiki jest grupowy **Discord** jednak jeśli ktoś zna się na **git** i/lub **GitLab** na tyle by używać narzędzi **"Issue" i "Merge Request"** można **używać też tych narzędzi**.

## Licencja

Zawartość tego repozytorium udostępniana jest na podstawie licencji [**CC BY-SA 4.0**](http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1). Pełny tekst dostępny w pliku  [LICENSE.md](./LICENSE.md).

This work is licensed under [**CC BY-SA 4.0**](http://creativecommons.org/licenses/by-sa/4.0/?ref=chooser-v1) 
![Creative Commons](https://mirrors.creativecommons.org/presskit/icons/cc.svg?ref=chooser-v1) ![Credit author](https://mirrors.creativecommons.org/presskit/icons/by.svg?ref=chooser-v1) ![Share alike](https://mirrors.creativecommons.org/presskit/icons/sa.svg?ref=chooser-v1). Full text of this license is available in [LICENSE.md](./LICENSE.md).